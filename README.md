# README #

### What is this repository for? ###

I did have 2 questions on the test which I didn't get a chance to ask with it being over the weekend so had to make a decision and get on with the task at hand.
I couldn't see a need to use the hotels.test file in the test pack. Having done some research these files can be created when moving from Windows to Mac and vice versa. Given I had everything needed with the hotels.json file I ignored this file.
In the readme.txt it says to filter and sort the data based on trpRating but I couldn't see this property in the .json file of data provided so have left it out. I could have interpreted this to me 'trip advisor rating' and used their api to pull data in, however this would have been a big assumption and didnt think it was right in the scope of the test.
The project can be fetched & checked out on this link: https://bitbucket.org/willhelmIII/test-travel-republic/src . I have also uploaded it to a url for quick viewing: http://www.willyoxall.co.uk/test-travel-republic .


I chose to use a tech stack including angular, bootstrap and firebase, with bower and grunt as dependancy/task managers. 
The most performance efficient solution would have been to read in a local .json file of hotels and then sort/filter the data using Angular. I didnt think this represented a real world situation as you would normally interact with a database to retrieve the data. With this in mind I thought it best to use firebase to showcase this skill set and to give myself the extra challenge to only get 100 records of data at any given time (exception is filtering in final solution) and paginate through, with performance on large data sets at the forefront of my mind.

Apart from the 2 question points above, I think I have completed all requirements given, and kept in mind all points to consider. I decided to concentrate more on the functionality over the styling, whilst ensuring the basic layout looked good and it was fully responsive. I have many examples of advance styling I can show if needed.

If I had more time (not under test conditions) I would have:
Enabled pagination for filters.
Improved UI for filter/sort options. Nice icons, hover effects.
Added search for 'Name' filtering.
Added more fancy styling to hotel entries.


### How do I get set up? ###

'cd' into the folder created on checkout and then run 'npm i' and then 'node server.js', and finally navigate to 'localhost:3000' in a browser.


### Contribution guidelines ###


### Who do I talk to? ###

Will Yoxall
willyoxall@gmail.com