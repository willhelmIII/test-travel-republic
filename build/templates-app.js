angular.module('templates-app', ['home/home.tpl.html']);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "<div class=\"filter-options row\">\n" +
    "  <h3 class=\" col-xs-12 col-sm-12 col-md-12 col-lg-12\">Filter Options</h3>\n" +
    "  <div class=\" col-xs-12 col-sm-3 col-md-2 col-lg-2 col-lg-offset-2  col-md-offset-2\">\n" +
    "    Name:<br/>\n" +
    "    <select ng-model=\"nameFilter\" ng-change=\"filterChange('Name')\">\n" +
    "      <option class=\"dropdown-item\" ng-repeat=\"x in alphabet\" value=\"{{x.value}}\">{{x.rating}}</option>\n" +
    "    </select>\n" +
    "  </div>\n" +
    "  <div class=\" col-xs-12 col-sm-3 col-md-2 col-lg-2\">\n" +
    "    Stars: <br/>\n" +
    "    <select ng-model=\"starsFilter\" ng-change=\"filterChange('Stars')\">\n" +
    "      <option class=\"dropdown-item\" ng-repeat=\"x in stars\" value=\"{{x.value}}\">{{x.rating}}</option>\n" +
    "    </select>\n" +
    "  </div>\n" +
    "  <div class=\" col-xs-12 col-sm-3 col-md-2 col-lg-2\">User Rating: <br/>\n" +
    "    <select ng-model=\"userRatingFilter\" ng-change=\"filterChange('UserRating')\">\n" +
    "      <option class=\"dropdown-item\" ng-repeat=\"x in userRatings\" value=\"{{x.value}}\">{{x.rating}}</option>\n" +
    "    </select>\n" +
    "  </div>\n" +
    "  <div class=\" col-xs-12 col-sm-3 col-md-2 col-lg-2\">Minimum Cost: <br/>\n" +
    "    <select ng-model=\"minCostFilter\" ng-change=\"filterChange('MinCost')\">\n" +
    "      <option class=\"dropdown-item\" ng-repeat=\"x in minCost\" value=\"{{x.value}}\">{{x.rating}}</option>\n" +
    "    </select>\n" +
    "  </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"sorting-options row\">\n" +
    "  <h3 class=\" col-xs-12 col-sm-12 col-md-12 col-lg-12\">Sorting Options</h3>\n" +
    "  <div class=\" col-xs-12 col-sm-3 col-md-2 col-lg-2 col-lg-offset-2  col-md-offset-2\">Stars:<br/> \n" +
    "    <div class=\"fa fa-sort-amount-asc btn-default btn-sm\" ng-class=\"{'active':sortMethod === 'Stars' && sortOption === 'desc'}\" ng-click=\"sortData('Stars', 'desc')\"></div>\n" +
    "    <div class=\"fa fa-sort-amount-desc btn-default btn-sm\" ng-class=\"{'active':sortMethod === 'Stars' && sortOption === 'asc'}\" ng-click=\"sortData('Stars', 'asc')\"></div>\n" +
    "  </div>\n" +
    "  <div class=\" col-xs-12 col-sm-3 col-md-2 col-lg-2\">Distance:<br/>\n" +
    "    <div class=\"fa fa-sort-amount-asc btn-default btn-sm\" ng-class=\"{'active':sortMethod === 'Distance' && sortOption === 'desc'}\" ng-click=\"sortData('Distance', 'desc')\"></div>\n" +
    "    <div class=\"fa fa-sort-amount-desc btn-default btn-sm\" ng-class=\"{'active':sortMethod === 'Distance' && sortOption === 'asc'}\" ng-click=\"sortData('Distance', 'asc')\"></div>\n" +
    "  </div>\n" +
    "\n" +
    "  <div class=\" col-xs-12 col-sm-3 col-md-2 col-lg-2\">\n" +
    "    Minimum Cost: <br/>\n" +
    "    <div class=\"fa fa-sort-amount-asc btn-default btn-sm\" ng-class=\"{'active':sortMethod === 'MinCost' && sortOption === 'desc'}\" ng-click=\"sortData('MinCost', 'desc')\"></div>\n" +
    "    <div class=\"fa fa-sort-amount-desc btn-default btn-sm\" ng-class=\"{'active':sortMethod === 'MinCost' && sortOption === 'asc'}\" ng-click=\"sortData('MinCost', 'asc')\"></div>\n" +
    "  </div>\n" +
    "  <div class=\" col-xs-12 col-sm-3 col-md-2 col-lg-2\">\n" +
    "    User Rating: <br/>\n" +
    "    <div class=\"fa fa-sort-amount-asc btn-default btn-sm\" ng-class=\"{'active':sortMethod === 'UserRating' && sortOption === 'desc'}\" ng-click=\"sortData('UserRating', 'desc')\"></div>\n" +
    "    <div class=\"fa fa-sort-amount-desc btn-default btn-sm\" ng-class=\"{'active':sortMethod === 'UserRating' && sortOption === 'asc'}\" ng-click=\"sortData('UserRating', 'asc')\"></div>\n" +
    "  </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"pagination\"> \n" +
    "  <div class=\"previous btn-default btn-lg\" ng-class=\"previousInactive\" ng-click=\"getPrevious100()\"><span class=\"fa fa-chevron-left\" aria-hidden=\"true\"></span>  Previous</div>\n" +
    "  <div class=\"next btn-default btn-lg\" ng-class=\"nextInactive\" ng-click=\"getNext100()\">Next <span class=\"fa fa-chevron-right\" aria-hidden=\"true\"></span></div>\n" +
    "  <div id=\"num-pages\"> Page {{pageNo}} of {{noOfPages}}</div>\n" +
    "  <div class=\"clearfix\"></div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"results row\" >\n" +
    "    <div ng-repeat=\"est in establishments\">\n" +
    "        <div class=\"establishment col-xs-6 col-sm-4 col-md-3 col-lg-3\">\n" +
    "           <div class=\"inner\">\n" +
    "              <h1>{{est.Name}}</h1>\n" +
    "              <img ng-src=\"{{est.ThumbnailUrl}}\"/>\n" +
    "              <div class=\"text-container\">\n" +
    "                <p>Type: {{est.EstablishmentType}}</p>\n" +
    "                <p>Location: {{est.Location}}</p>\n" +
    "                <p>Distance: {{est.Distance}}</p>\n" +
    "                <p>Minimum Cost: {{est.MinCost}}</p>\n" +
    "                <p>Stars: {{est.Stars}}</p>\n" +
    "                <p>User Rating: {{est.UserRating}}</p>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "           \n" +
    "        </div>\n" +
    "        <div class=\"clearfix visible-lg\" ng-if=\"($index + 1) % 4 == 0\"></div>\n" +
    "        <div class=\"clearfix visible-md\" ng-if=\"($index + 1) % 4 == 0\"></div>\n" +
    "        <div class=\"clearfix visible-sm\" ng-if=\"($index + 1) % 3 == 0\"></div>\n" +
    "        <div class=\"clearfix visible-xs\" ng-if=\"($index + 1) % 2 == 0\"></div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"pagination\"> \n" +
    "  <div class=\"previous btn-default btn-lg\" ng-class=\"previousInactive\" ng-click=\"getPrevious100()\"><span class=\"fa fa-chevron-left\" aria-hidden=\"true\"></span>  Previous</div>\n" +
    "  <div class=\"next btn-default btn-lg\" ng-class=\"nextInactive\" ng-click=\"getNext100()\">Next <span class=\"fa fa-chevron-right\" aria-hidden=\"true\"></span></div>\n" +
    "  <div id=\"num-pages\"> Page {{pageNo}} of {{noOfPages}}</div>\n" +
    "  <div class=\"clearfix\"></div>\n" +
    "</div>");
}]);
