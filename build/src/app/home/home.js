
angular.module( 'ngBoilerplate.home', [
  'ui.router'
])


.config(function config( $stateProvider ) {
  $stateProvider.state( 'home', {
    url: '/home',
    views: {
      "main": {
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'
      }
    },
    data:{ pageTitle: 'Travel Repulblic Test' }
  });
})

.controller( 'HomeCtrl', function HomeController( $scope ) {

  // set up vars
  $scope.establishments = [];
  $scope.sortMethod = 'Distance';
  $scope.sortOption = 'asc';
  $scope.previousInactive = 'inactive';
  $scope.paginationIndex = 0;  
  $scope.pageNo = $scope.paginationIndex+1;
  $scope.stars = [{rating:"0 Star", value:0}, {rating:"1 Star", value:1}, {rating: "2 Stars", value:2},   {rating: "3 Stars", value:3},  {rating: "4 Stars", value:4},  {rating: "5 Stars", value:5}];
  $scope.userRatings = [{rating:"0 out 0f 10", value:0}, {rating:"1 out 0f 10", value:1}, {rating:"2 out 0f 10", value:2}, {rating:"3 out 0f 10", value:3}, {rating:"4 out 0f 10", value:4},  {rating:"5 out 0f 10", value:5},{rating:"6 out 0f 10", value:6}, {rating:"7 out 0f 10", value:7}, {rating:"8 out 0f 10", value:8}, {rating:"9 out 0f 10", value:9}, {rating:"10 out 0f 10", value:10}];
  $scope.alphabet = [{rating:"Starts with A", value:'A'}, {rating:"Starts with B", value:'B'}, {rating:"Starts with C", value:'C'}, {rating:"Starts with D", value:'D'}, {rating:"Starts with E", value:'E'}, {rating:"Starts with F", value:'F'},{rating:"Starts with G", value:'G'}, {rating:"Starts with H", value:'H'}, {rating:"Starts with I", value:'I'}, {rating:"Starts with J", value:'J'}, {rating:"Starts with K", value:'K'}, {rating:"Starts with L", value:'L'}, {rating:"Starts with M", value:'M'},{rating:"Starts with N", value:'N'}, {rating:"Starts with O", value:'O'},{rating:"Starts with P", value:'P'},{rating:"Starts with Q", value:'Q'},{rating:"Starts with R", value:'R'},{rating:"Starts with S", value:'S'},{rating:"Starts with T", value:'T'},{rating:"Starts with U", value:'U'},{rating:"Starts with V", value:'V'},{rating:"Starts with W", value:'W'}, {rating:"Starts with X", value:'X'},{rating:"Starts with Y", value:'Y'}, {rating:"Starts with Z", value:'Z'}];
  $scope.minCost = [{rating:"£0 - £500", value:'0-500'}, {rating:"£500 - £1000", value:'500-1000'}, {rating:"£1000 - £1500", value:'1000-1500'}, {rating:"£1500 - £2000", value:'1500-2000'},  {rating: "£2000 - £3000", value:'2000-3000'},  {rating:"£3000 - £4000", value:'3000-4000'},{rating:"£4000+", value:'4000+'}];

  // set up firebase
  var config = {
    apiKey: "AIzaSyAvBNBS6nS3gEwdn1Ll9PwSkDq0tEehArM",
    authDomain: "test-travel-republic.firebaseapp.com",
    databaseURL: "https://test-travel-republic.firebaseio.com",
    projectId: "test-travel-republic",
    storageBucket: "test-travel-republic.appspot.com",
    messagingSenderId: "563158440616"
  };
  firebase.initializeApp(config);
  $scope.firebaseEstablishmentsRef = firebase.database().ref().child('Establishments');
  
  // get total records for pagination
  $scope.firebaseEstablishmentsRef.orderByKey().limitToLast(1).once('value', function(snap){
    snap.forEach(function(snap) {
      var totalNoEst = snap.key;
      $scope.noOfPages = parseInt(totalNoEst/100 +1, 10);
    });      
  });     



  // get next 100 items for pagination
  $scope.getNext100 = function() {
    //check if db query active, if so return
    if($scope.searchActive){
      return;
    }
    // check if on last page if so return set css class and return.
    if ($scope.establishments.length < 100) {
      return;
    }

    // define function and set scope vars.
    $scope.previousInactive = '';
    $scope.searchActive = true;
    var newEstablishments = [];
    var startPointValue, startPointKey, firebaseRef;
    $scope.paginationIndex += 1;
    $scope.pageNo = $scope.paginationIndex+1;

    // get last sort value - better ways to do this
    if($scope.sortMethod == 'Distance'){
      startPointValue = $scope.establishments[$scope.establishments.length-1].Distance;
    } else if($scope.sortMethod == 'Stars'){
      startPointValue = $scope.establishments[$scope.establishments.length-1].Stars;
    } else if($scope.sortMethod == 'MinCost'){
      startPointValue = $scope.establishments[$scope.establishments.length-1].MinCost;
    } else if($scope.sortMethod == 'UserRating'){
      startPointValue = $scope.establishments[$scope.establishments.length-1].UserRating;
    } else if($scope.sortMethod == 'Name'){
      startPointValue = $scope.establishments[$scope.establishments.length-1].Namw;
    }

    // get last sort key
    startPointKey = $scope.newKeys[$scope.newKeys.length-1];

    // set firebaseRef depending on if asc or desc
    if($scope.sortOption == 'asc'){
      firebaseRef = $scope.firebaseEstablishmentsRef.orderByChild($scope.sortMethod).startAt(startPointValue, startPointKey).limitToFirst(101);
    } else {
      firebaseRef = $scope.firebaseEstablishmentsRef.orderByChild($scope.sortMethod).endAt(startPointValue, startPointKey).limitToLast(100);
    }

    // get and update data
    $scope.getData(firebaseRef);         
  };

  $scope.getPrevious100 = function() {
    //check if db query active, if so return
    if($scope.searchActive || $scope.previousInactive == 'inactive') {
      return;
    }

    // define function and set scope vars.
    $scope.nextInactive = '';
    $scope.paginationIndex -= 1;
    $scope.pageNo = $scope.paginationIndex+1;
    var startPointValue, startPointKey, firebaseRef;
    if ($scope.paginationIndex === 0) {
      $scope.previousInactive = 'inactive';
    } 

    // get last sort value - better ways to do this
    if($scope.sortMethod == 'Distance'){
      startPointValue = $scope.establishments[0].Distance;
    } else if($scope.sortMethod == 'Stars'){
      startPointValue = $scope.establishments[0].Stars;
    } else if($scope.sortMethod == 'MinCost'){
      startPointValue = $scope.establishments[0].MinCost;
    } else if($scope.sortMethod == 'UserRating'){
      startPointValue = $scope.establishments[0].UserRating;
    } else if($scope.sortMethod == 'Name'){
      startPointValue = $scope.establishments[0].Name;
    }

    // get last sort key
    startPointKey = $scope.newKeys[0];

    // set firebaseRef depending on if asc or desc
    if($scope.sortOption == 'asc'){
      firebaseRef = $scope.firebaseEstablishmentsRef.orderByChild($scope.sortMethod).endAt(startPointValue, startPointKey).limitToLast(100);
    } else {
      firebaseRef = $scope.firebaseEstablishmentsRef.orderByChild($scope.sortMethod).startAt(startPointValue, startPointKey).limitToFirst(101);
    }    

    // get and update data
    $scope.getData(firebaseRef);    
  };  

  // filter data event listener - makes pagination inactive as smaller data sets
  $scope.filterChange = function(changeValue){
    // set scope vars
    $scope.sortMethod = changeValue;
    $scope.nextInactive = 'inactive';

    // depending on sort method construct firebaseRef
    if($scope.sortMethod == 'Stars'){
      firebaseRef = $scope.firebaseEstablishmentsRef.orderByChild($scope.sortMethod).equalTo(parseInt($scope.starsFilter, 10));      
    } else if($scope.sortMethod == 'UserRating') {
      firebaseRef = $scope.firebaseEstablishmentsRef.orderByChild($scope.sortMethod).startAt(parseInt($scope.userRatingFilter, 10)).endAt(parseInt($scope.userRatingFilter, 10)+1);      
    } else if($scope.sortMethod == 'Name'){
      var index = 0;
      var nextLetter;
      $scope.alphabet.forEach(function(alphabetItem, index){
        if(alphabetItem.value == $scope.nameFilter){
          nextLetter = $scope.alphabet[index+1].value;
        }
      });
      firebaseRef = $scope.firebaseEstablishmentsRef.orderByChild($scope.sortMethod).startAt($scope.nameFilter).endAt(nextLetter);      
    } else if($scope.sortMethod == 'MinCost'){
      $scope.sortOption = '';
      if($scope.minCostFilter == '4000+'){
        firebaseRef = $scope.firebaseEstablishmentsRef.orderByChild($scope.sortMethod).startAt(4000);      
      } else {
        var startPoint = $scope.minCostFilter.split(/\s*\-\s*/g)[0];
        var endPoint = $scope.minCostFilter.split(/\s*\-\s*/g)[1];
        firebaseRef = $scope.firebaseEstablishmentsRef.orderByChild($scope.sortMethod).startAt(parseInt(startPoint, 10)).endAt(parseInt(endPoint, 10));      
      }      

    }

    // get and update data
    $scope.getData(firebaseRef);
  
  };

  // get data from firebase and update page via scope
  $scope.getData = function(firebaseRef){
    // set up vars and scope variables
    // Use newEstablishments so page ata gets updated in one go
    var newEstablishments = [];
    $scope.newKeys = [];
    $scope.searchActive = true;

    // get data from firebase
    firebaseRef.once('value', function(snap){
      // console.log('Number of results: ' +snap.numChildren());
      snap.forEach(function(establishment) {

        $scope.newKeys.push(establishment.key);
        newEstablishments.push(establishment.val());  

        // Thought about rounding distance purley from a UI perspective. Decided against this.
        // newEstablishments[newEstablishments.length-1].Distance = newEstablishments[newEstablishments.length-1].Distance.toFixed(2);

      });

      // If sort desc, reverse items
      if($scope.sortOption == 'desc'){
        newEstablishments = newEstablishments.reverse();
        $scope.newKeys = $scope.newKeys.reverse();
        
      }

      // work around for firebase startAt logic
      if(newEstablishments.length == 101){
        newEstablishments.splice(0,1);
      }
      $scope.establishments = newEstablishments;

      // Enable pagination buttons again and check if on last page
      $scope.searchActive = false;
      if ($scope.establishments.length < 100) {
        $scope.nextInactive = 'inactive';
      }          

      $scope.$apply();

    });     
  };

  // Sort data event listener
  $scope.sortData = function(sortMethod, option){
    $scope.sortMethod = sortMethod;
    $scope.sortOption = option;
    $scope.paginationIndex = 0;
    $scope.pageNo = 1;
    var firebaseRef;
    if(option == 'asc'){

      firebaseRef = $scope.firebaseEstablishmentsRef.orderByChild($scope.sortMethod).limitToFirst(100);
    } else if(option == 'desc'){
      firebaseRef = $scope.firebaseEstablishmentsRef.orderByChild($scope.sortMethod).limitToLast(100);
    }
    $scope.getData(firebaseRef);
    $scope.previousInactive = 'inactive';    
    $scope.nextInactive = '';   
  };


  //Initialise 
  $scope.sortData('Stars',  'desc');
})

;

